%title: ArgoEvents
%author: xavki
%Vidéos: [ArgoEvents]()
%blog: [Xavki Blog](https://xavki.blog)


# ARGO Events : Introduction & Concepts


<br>

Projet Argo - Kubernetes

Site : https://argoproj.github.io/


---------------------------------------------------------------------

# Projet ARGO


		* workflow : orchestration de job k8s
		* continuous deployment (CD) : gitops deploiement
		* rollout : stratégies de déploiements
		* events : déclenchement de ressources k8s sur évènements
				* connexion à des mqtt, webhook etc
				* évènement cloud
				* option > trigger des outils externes k8s
				

---------------------------------------------------------------------


# ARGO Events : Introduction & Concepts


<br>

	PRINCIPE :


1 - outil d'évènements : queues, webhook, s3...

2 - écoute des évènements

3 - bus d'évènements interne (NATS streaming)

4 - déclenchement d'une action > dans k8s, cloud...


---------------------------------------------------------------------


# ARGO Events : Introduction & Concepts


<br>

	EVENT SOURCE :

		* ressource kubernetes

		* connexion à la source d'évènement
				* 24 types actuellement
				* amqp, mqtt, kafka, minio, gitlab/hub, sns, slack, redis...

		* basé sur CloudEvents : https://github.com/cloudevents/spec


---------------------------------------------------------------------


# ARGO Events : Introduction & Concepts


<br>

	SENSOR :

		* écoute l'event bus

		* se connecte aux events d'une dépendance (dependancy)
				* lien avec Event Source

		* exécute un trigger

		* EventSource > Sensor

		* Sensor = dependency + trigger

		* multiples dependencies possibles

---------------------------------------------------------------------


# ARGO Events : Introduction & Concepts


<br>

	TRIGGER :

		* action délcenchée par le sensor suite à un event

		* 12 types :
				* tout object kubernetes
				* lambda aws
				* http request
				* kafka
				* slack
				* custom...

		* templating et conditions


---------------------------------------------------------------------


# ARGO Events : Introduction & Concepts


<br>

	METRICS : 

			* récupération des métriques pour prometheus
					* kubernetes_sd_configs

	HA Preco :

			* persistence des datas des pods d'eventbus

			* antiaffinity

			* eviction des pods (nats en dernier)

			* différenciation des namespaces possibles
				*	attention au service account


