%title: ArgoEvents
%author: xavki
%Vidéos: [ArgoEvents]()
%blog: [Xavki Blog](https://xavki.blog)


# ARGO Events : RabbitMQ

<br>

 ingress >	rabbitmq >> eventsource > sensor/dependency > trigger > pod

-------------------------------------------------------------------------

# ARGO Events : RabbitMQ


<br>

INSTALLATION DE RABBITMQ 

<br>

* ajout du dépôt bitnami

```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

-------------------------------------------------------------------------

# ARGO Events : RabbitMQ

<br>

* configuration (exceptionnellement pas de persistence)

```
persistence:
  enabled: false
```

<br>

* installation

```
helm install my-release bitnami/rabbitmq -f values.yaml
```

-------------------------------------------------------------------------

# ARGO Events : RabbitMQ

<br>

* création des identifiants (normalement)

```
apiVersion: v1
kind: Secret
metadata:
  name: mysecret
type: Opaque
data:
  username: dXNlcg==
  password: ZlBmbjBkVFBORw==
```

-------------------------------------------------------------------------

# ARGO Events : RabbitMQ

<br>

ARGOEVENTS

<br>

kubectl port-forward --namespace rabbit svc/rabbit-rabbitmq 5672:5672

* création de l'eventsource

* création du sensor

* injection python
