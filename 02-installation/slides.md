%title: ArgoEvents
%author: xavki
%Vidéos: [ArgoEvents]()
%blog: [Xavki Blog](https://xavki.blog)


# ARGO Events : Installation

<br>

INSTALLATION FULL NAMESPACE

<br>

* création d'un namespace dédié

```
kubectl create ns argo-events
```

------------------------------------------------------------

# ARGO Events : Installation


<br>

* ajout des CRDs & des roles

```
kubectl apply -n argo-events -f https://raw.githubusercontent.com/argoproj/argo-events/stable/manifests/install.yaml
```


<br>

* ajout du NATS pour l'eventbus

```
kubectl apply -n argo-events -f https://raw.githubusercontent.com/argoproj/argo-events/stable/examples/eventbus/native.yaml
```

------------------------------------------------------------

# ARGO Events : Installation

<br>

INSTALLATION HELM

<br>

* création d'un namespace dédié

```
kubectl create ns argo-events
```

<br>

* install de la chart

```
helm repo add argo https://argoproj.github.io/argo-helm
helm install argo-events argo/argo-events
```

* ajout du NATS pour l'eventbus

```
kubectl apply -n argo-events -f https://raw.githubusercontent.com/argoproj/argo-events/stable/examples/eventbus/native.yaml
```

------------------------------------------------------------

# ARGO Events : Installation


* remove argo-events

```
kubectl patch eventbus/default -p '{"metadata":{"finalizers":[]}}' --type=merge -n argo-events
```
