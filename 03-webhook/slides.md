%title: ArgoEvents
%author: xavki
%Vidéos: [ArgoEvents]()
%blog: [Xavki Blog](https://xavki.blog)


# ARGO Events : WebHook

<br>

	ingress >> svc >> eventsource > sensor/dependency > trigger > pod

<br>

curl -d '{"command":"cat","arg0":"/etc/hosts"}' -H "Content-Type: application/json" -X POST http://webhook.kub/

